export default {
    replaceCart(state) {
        var items = localStorage.getItem('cart');
        if (items) items = JSON.parse(items)
        state.cart = items ?? [];
    },

    addToCart(state, cartProduct) {
        let product = cartProduct.product;
        let type = cartProduct.type;
        state.cart.push({ ...product, totalQty: 1, totalAmount: parseInt(type == 'bulk' ? product.bulk_price : product.price), buyingBulk: type == 'bulk' ? 1 : 0 });
        localStorage.setItem('cart', JSON.stringify(state.cart));
    },

    increaseCart(state, cartProduct) {
        let product = cartProduct.product;
        let type = cartProduct.type;
        let itemExist = false;
        state.cart.map((item) => {
            if (item.id === product.id) {
                itemExist = true;
                item.totalQty++
                item.totalAmount = item.buyingBulk == 1 ? item.bulk_price * item.totalQty : item.price * item.totalQty;

            }
            return item;
        })
        if (!itemExist) {
            state.cart.push({ ...product, totalQty: 1, totalAmount: parseInt(type == 'bulk' ? parseInt(product.bulk_price) : parseInt(product.price)), buyingBulk: type == 'bulk' ? 1 : 0 });

        }

        localStorage.setItem('cart', JSON.stringify(state.cart));

    },

    decreaseCart(state, cartProduct) {
        let product = cartProduct.product;
        state.cart.map((item) => {
            if (item.id === product.id) {
                if (item.totalQty > 1) {
                    item.totalQty--
                    item.totalAmount = item.buyingBulk == 1 ? item.bulk_price * item.totalQty : item.price * item.totalQty;
                }
                else {
                    console.log('not avaialable');
                    state.totalCartLength = state.totalCartLength - parseInt(product.totalQty);
                    state.cart = state.cart.filter((item) => item.id != product.id);
                    localStorage.setItem('cart', JSON.stringify(state.cart));
                }
                return item;
            }
        })
    },

    updateCartLength(state) {
        let subTotal = []
        state.cart.map((item) => {
            subTotal.push(item.totalQty)
            return subTotal;
        })
        state.totalCartLength = subTotal.reduce((sum, current) => sum + current, 0)
    },


    updateCartPrice(state) {
        let subTotal = []
        state.cart.map((item) => {
            subTotal.push(item.totalAmount)
            return subTotal;
        })
        state.totalCartAmount = subTotal.reduce((sum, current) => sum + current, 0)
    },


    removeFromCart(state, cartProduct) {
        let product = cartProduct.product;
        state.cart = state.cart.filter((item) => item.id != product.id);
        localStorage.setItem('cart', JSON.stringify(state.cart));
    },
}