import axios from 'axios'

export default{
    handleError(context, error){
        if (typeof error == 'object') {
            if(error.response.status == 422){
                var err = error.response.data;
                var msg = err.message;
                context.commit('setNotification', {type : 2, message: msg});
                return msg;
            }
            else if (error.response.status == 303) {
                resp = JSON.parse(error.response);
                console.log(resp);
                context.commit('setNotification', { type: 2, message: resp.error });
            } 
            else if (error.response.status == 404) {
                resp = JSON.parse(error.response);
                msg = 'response not found';
                context.commit('setNotification', { type: 2, message: msg });
            } 
            else if (error.response.status == 400) {
                resp = JSON.parse(error.response);
                msg = resp.msg;
                context.commit('setNotification', { type: 2, message: msg });
            } 
            else if (error.response.status == 401) {
                msg = 'Oops! Authentication error, Please login again';
                context.commit('setNotification', { type: 2, message: msg });
                context.commit('logout');
            } 
            else {
               if(error.response.status == 422){
            var err = error.response.data;
            var msg = err.message;
            context.commit('setNotification', {type : 2, message: msg});
            return msg;
        }
        else if (error.response.status == 303) {
            resp = JSON.parse(error.response);
            console.log(resp);
            context.commit('setNotification', { type: 2, message: resp.error });
        } 
        else if (error.response.status == 404) {
            resp = JSON.parse(error.response);
            msg = 'response not found';
            context.commit('setNotification', { type: 2, message: msg });
        } 
        else if (error.response.status == 400) {
            resp = JSON.parse(error.response);
            msg = resp.msg;
            context.commit('setNotification', { type: 2, message: msg });
        } 
        else if (error.response.status == 401) {
            msg = 'Oops! Authentication error, Please login again';
            context.commit('setNotification', { type: 2, message: msg });
            context.commit('logout');
        } 
        else {
            msg = 'Oops! server error, Please try again';
            context.commit('setNotification', { type: 2, message: msg });
        }
            }
        }else{
            context.commit('setNotification', { type: 2, message: error });
        }
        
    },
    
    post(context, data){
        return new Promise((resolve, reject) => {
            axios.post(context.state.endpoint + data.endpoint, data.details, {
                headers: { Authorization: 'Bearer ' + context.state.token,
                }
            })
            .then((data)=>{

                resolve(data);
            })
            .catch((error)=>{
                reject(error);
            })
        });
    },
    put(context, data){
        return new Promise((resolve, reject) => {
            axios.put(context.state.endpoint + data.endpoint, data.details, {
                headers: { Authorization: 'Bearer ' + context.state.token,
                }
            })
            .then((data)=>{
                resolve(data);
            })
            .catch((error)=>{
                console.log(error);
                context.dispatch('handleError', error);
                reject(error);
            })
        });
    },
    get(context, endpoint){
        return new Promise((resolve, reject) => {
            axios.get(context.state.endpoint + endpoint, {
                headers: {
                    Authorization: 'Bearer ' + context.state.token
                },
            })
            .then((data)=>{
                resolve(data);
            })
            .catch((error)=>{
                // context.dispatch('handleError', error);
                reject(error);
            })
        });
    },
   
}