import { createStore } from 'vuex'
import mutations from './mutations';
import actions from './actions';
import getters from './getters';

export default createStore({
  state: {
    endpoint: process.env.VUE_APP_ENDPOINT,
    notification: {
        type: 0,
        message: 'a notification'
    },
    categories: [],
    products: [],
    user: {},
    token: '',
    cart:[],
    totalCartLength:0,
    totalCartAmount:0,
    productQuery: ''
},
  getters,
  mutations,
  actions
})
