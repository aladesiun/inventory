import { createRouter, createWebHistory } from 'vue-router'
import Layout from '../views/layout'
import Signin from '../views/auth/singin.vue'
import Shop from '../views/product/shop/shop.vue';
import Home from '../views/Home.vue'
const routes = [
  // {
  //   path: '/about',
  //   name: 'about',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  // },
  {
    path: '/',
    name: 'Layout',
    component: Layout,
    meta:{
      AuthRequired:true
  },
  children:[
    {
      path: '/shop',
      name: 'Shop',
      component: Shop
    },
    {
      path: '/',
      name: 'Home',
      component: Home
    },

  ]
  },
  {
    path: '/signin',
    name: 'Signin',
    component: Signin
  },
 
  
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router;